# my3dtest

>我的一个测试Vuejs和Threejs结合的小Demo，具体介绍可以参照博客：https://blog.csdn.net/weixin_38298363/article/details/124386976

效果图如下：

![输入图片说明](src/assets/%E4%BC%81%E4%B8%9A%E5%BE%AE%E4%BF%A1%E6%88%AA%E5%9B%BE_16507905883377.png)

## 操作步骤
``` bash
# 1.下载项目到本地：
git clone https://gitee.com/woailbl/my3dtest.git

# 2. 执行安装依赖包
npm install

# 3. 运行之后，访问 http://localhost:8080
npm run dev

# 4.打包
npm run build

```
本次修改部分：

多增加两个几何体：

点击几何体的时候会打印出其名称：

![点击几何体](static/%E5%BE%AE%E4%BF%A1%E6%88%AA%E5%9B%BE_20220425205340.png)

点击空白处：弹出窗口提示

![点击几何体](static/%E5%BE%AE%E4%BF%A1%E6%88%AA%E5%9B%BE_20220425205353.png)


