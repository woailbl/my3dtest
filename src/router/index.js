import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import My3D001 from '../views/My3D001'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'My3D001',
      component: My3D001,
    }
  ]
})
